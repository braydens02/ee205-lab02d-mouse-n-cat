###############################################################################
###          University of Hawaii, College of Engineering
### @brief   Lab 02d - Mouse N Cat- EE 205 - Spr 2022
###
### @file    Makefile
### @version 1.0 - Initial version
###
### Build a Hello World C program
###
### @author  Brayden Suzuki <braydens@hawaii.edu>
### @date    24 Jan 2022
###
### @see     https://www.gnu.org/software/make/manual/make.html
###############################################################################

CC     = g++
CFLAGS = -g -Wall

TARGET = mouseNcat

all: $(TARGET)

hello: mouseNcat.cpp
		$(CC) $(CFLAGS) -o $(TARGET) mouseNcat.cpp

test: mouseNcat
		./mouseNcat

clean:
		rm -f $(TARGET) *.o

